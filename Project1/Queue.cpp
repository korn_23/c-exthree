#include "Queue.h"
#include <stdexcept>

Queue::Queue(int size) {
	flag = true;
	arr = new double[size];
	this->size = size;
	firstPointer = 0;
	lastPointer = 0;
}

Queue::Queue(const Queue &copy) {
	flag = copy.flag;
	size = copy.size;
	firstPointer = copy.firstPointer;
	lastPointer = copy.lastPointer;
	arr = new double[copy.size];
	for (int i = 0; i < size; i++) {
		arr[i] = copy.arr[i];
	}
}

void Queue::addElement(double element) {
	//�������� �� ������������ 
	if ((lastPointer + 1) % size == firstPointer) {
		throw new std::runtime_error("Size error");
	}
	//�������� �� ������ �������
	if (flag) {
		arr[lastPointer] = element;
		flag = false;
	}
	else {
		lastPointer++;
		lastPointer = lastPointer % size;
		arr[lastPointer] = element;
	}
}

double Queue::getElem() {
	if (flag) {
		throw new std::runtime_error("Empty error");
	}
	if (firstPointer == lastPointer) {
		flag = true;
		return arr[firstPointer];
	}
	if (firstPointer != lastPointer) {
		firstPointer++;
		return arr[firstPointer - 1];
	}
}

double Queue::checkFirstElem() {
	if (flag) {
		throw new std::runtime_error("Empty error");
	}
	return arr[firstPointer];
}
int Queue::getSize() {
	return size;
}
void Queue::makeEmpty() {
	firstPointer = lastPointer;
	flag = true;
}

bool Queue::isEmpty() {
	return flag;
}