#include "Queue.h"
#include "Iterator.h"
#include <iostream>

using namespace std;

int main() {
	
	setlocale(LC_ALL, "Russian");

	Queue *ringA = new Queue(5);

	ringA->addElement(1);
	ringA->addElement(2);
	ringA->addElement(3);

	
	Queue *ringB = new Queue(*ringA);

	ringB->addElement(4);
	
	Queue *ringC = new Queue(2);
	
	
	if (!(ringB->isEmpty())) 
	{
		cout << "queueB �� ������ " << endl<<endl;
	}

	if (ringC->isEmpty()) {
		cout << "queueC ������ " << endl;
	}
	
	Iterator iteratorA (ringA);
	
	Iterator iteratorB (ringB);
	
	while (!(iteratorA.finish())) 
	{
		iteratorA.next();
		cout << iteratorA.getValue() << endl;

	}

	cout << endl<<endl;

	while (!(iteratorB.finish())) 
	{
		iteratorB.next();
		cout << iteratorB.getValue() << endl;

	}

	cout << endl << endl;
	cout << "������ ringB :" << ringB->getSize() << endl;
	
	ringB->makeEmpty();
	
	if (ringB->isEmpty()) 
	{
		cout << "ringB ������ "<< endl;
	}
	
	iteratorA.start();
	
	cout << "ringA �� ������  ������� ��������" << endl;
	
	while (!iteratorA.finish()) 
	{
		iteratorA.next();
		cout << iteratorA.getValue() << endl;

	}
	
	cout << endl << endl;
	cout << "ringA ������ �������: " << ringA->getElem() << endl;
	
	iteratorA.start();
	
	cout << "ringA ����� ������ ������� ��������" << endl;
	
	while (!iteratorA.finish()) 
	{
		iteratorA.next();
		cout << iteratorA.getValue() << endl;
	}

	system("pause");
	delete ringA;
	delete ringB;
	delete ringC;

	return 0;
}